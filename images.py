import numpy as np
import matplotlib.image as mpimg 
import matplotlib.pyplot as plt 

def RGB2HSV_pix(pix):
    RGB_ = pixel
    Cmax = max(RGB_)
    Cmin = min(RGB_)
    Delta = Cmax-Cmin

    # Hue calculation
    if Delta == 0:
        H = 0
    elif Cmax == RGB_[0]:
        H = 60*(((RGB_[1]-RGB_[2])/Delta)%6)
    elif Cmax == RGB_[1]:
        H = 60*(((RGB_[2]-RGB_[0])/Delta)+2)
    elif Cmax == RGB_[2]:
        H = 60*(((RGB_[0]-RGB_[1])/Delta)+6)

    # Saturation Calculation
    if Cmax == 0:
        S = 0
    else:
        S = Delta/Cmax

    # Value Calculation
    V = Cmax

    return np.array([H,S,V])

def RGB2HSV(im):
	m,n = im.shape
	HSV = np.zeros((m,n,3))
	for i in range(m):
	    for j in range(n):
		HSV[i,j,3] = RGB2HSV_pix()
	return HSV

def load_image(path):
	return mpimg.imread(path)/255

def show_image(image1,image2,title1,title2):
	plt.figure()
	plt.subplot(111)
	plt.imshow(image1)
	plt.title(title1)
	plt.subplot(121)
	plt.imshow(image2)
	plt.title(title2)
	plt.show()	

rgb = load_image("image.png")
hsv = RGB2HSV(rgb)
print(hsv)
